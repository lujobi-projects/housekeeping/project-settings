# GitLab Project Settings Updater

This project is a simple script that updates the settings of all projects in a GitLab group.

## Setup

Ensure you have a `.env` file in the root directory of the project with the following content:
```env
GITLAB_PRIVATE_TOKEN=your_gitlab_private_token
GITLAB_GROUP_ID=your_gitlab_group_id
```

## Environment Variables
This is an alternative to the `.env` file setup.

- `GITLAB_PRIVATE_TOKEN`: Your GitLab private token.
- `GITLAB_GROUP_ID`: The ID of the GitLab group whose projects you want to update.

## License

This project is licensed under the MIT License.
