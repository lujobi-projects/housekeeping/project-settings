use dotenv::dotenv;
use gitlab::{
    api::{
        groups::{self, subgroups},
        projects, Query,
    },
    Gitlab,
};
use serde::{Deserialize, Serialize};

type Id = u64;

#[derive(Debug, Deserialize)]
struct ApiProjectInfo {
    name: String,
    web_url: String,
    default_branch: String,
    id: Id,
}

#[derive(Debug, Deserialize)]
struct ApiGroupInfo {
    full_name: String,
    id: Id,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
struct Project {
    id: Id,
    name: String,
    web_url: String,
    default_branch: String,
}

impl From<ApiProjectInfo> for Project {
    fn from(project_info: ApiProjectInfo) -> Self {
        Project {
            id: project_info.id,
            name: project_info.name,
            web_url: project_info.web_url,
            default_branch: project_info.default_branch,
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
struct Group {
    id: Id,
    full_name: String,
    projects: Vec<Project>,
    subgroups: Vec<Group>,
}

impl From<ApiGroupInfo> for Group {
    fn from(api_group: ApiGroupInfo) -> Self {
        Group {
            id: api_group.id,
            full_name: api_group.full_name,
            projects: Vec::new(),
            subgroups: Vec::new(),
        }
    }
}
#[derive(Debug, Serialize, Deserialize, Clone)]
struct Empty {}

fn update_project(client: &Gitlab, project: &ApiProjectInfo) -> Result<(), String> {
    println!("Updating project {}", project.name);
    let update_endpoint = projects::EditProject::builder()
        .project(project.id)
        .builds_access_level(projects::FeatureAccessLevel::Private)
        .build()
        .unwrap();
    let res: Result<Empty, String> = update_endpoint.query(client).map_err(|e| e.to_string());
    match res {
        Ok(_) => Ok(()),
        Err(e) => Err(format!("Error updating project {}: {}", project.name, e)),
    }
}

fn update_group(client: &Gitlab, id: Id) -> Result<(), Vec<String>> {
    let sub_group_endpoint = subgroups::GroupSubgroups::builder()
        .group(id)
        .build()
        .unwrap();
    let sub_group_info: Vec<ApiGroupInfo> = sub_group_endpoint.query(client).unwrap();

    let project_endpoint = groups::projects::GroupProjects::builder()
        .group(id)
        .build()
        .unwrap();
    let project_info: Vec<ApiProjectInfo> = project_endpoint.query(client).unwrap();

    let (_, project_errors): (_, Vec<_>) = project_info
        .into_iter()
        .map(|p| update_project(client, &p))
        .partition(Result::is_ok);
    let project_errors = project_errors
        .into_iter()
        .map(Result::unwrap_err)
        .collect::<Vec<_>>();

    let (_, group_errors): (_, Vec<_>) = sub_group_info
        .into_iter()
        .map(|g| update_group(client, g.id))
        .partition(Result::is_ok);
    let group_errors = group_errors
        .into_iter()
        .flat_map(Result::unwrap_err)
        .collect::<Vec<_>>();

    let all_errors: Vec<String> = [project_errors, group_errors].concat();

    if all_errors.is_empty() {
        Ok(())
    } else {
        Err(all_errors)
    }
}

fn get_root_group_id(client: &Gitlab, name: &str) -> Id {
    let group_endpoint = groups::Group::builder().group(name).build().unwrap();
    let group_info: ApiGroupInfo = group_endpoint.query(client).unwrap();
    group_info.id
}

fn main() {
    dotenv().ok();

    let gitlab_auth_token = std::env::var("GITLAB_PRIVATE_TOKEN")
        .expect("missing gitlab auth token in `GITLAB_PRIVATE_TOKEN` env variable");
    let gitlab_group_id = std::env::var("GITLAB_GROUP_ID")
        .expect("missing gitlab group id in `GITLAB_GROUP_ID` env variable");

    let client = Gitlab::new("gitlab.com", gitlab_auth_token).unwrap();
    let group_id = get_root_group_id(&client, gitlab_group_id.as_str());

    println!("Running for group {gitlab_group_id}");

    match update_group(&client, group_id) {
        Ok(()) => println!("Group updated successfully"),
        Err(errors) => {
            for error in errors {
                eprintln!("{error}");
            }
        }
    }
}
